const express = require('express');
const bodyParser = require('body-parser')
const misRutas = require ("./router/index");
const path = require ("path");

const app = express();

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.use(misRutas);
app.engine('html', require('ejs').renderFile);

const puerto = 1313;
app.listen(puerto,()=>{
    console.log('Iniciando puerto ' + puerto);
})