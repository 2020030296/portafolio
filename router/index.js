const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');

router.get('/', (req,res)=>{
    res.render('index.html');
})

router.get('/Proyectos', (req,res)=>{
    res.render('proyectos.html')
})
module.exports = router;